<?php

namespace Drupal\erf;

use Drupal\Core\Entity\BundlePermissionHandlerTrait;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\erf\Entity\RegistrationType;
use Drupal\erf\Entity\RegistrationTypeInterface;

/**
 * Provides dynamic permissions of the erf module.
 *
 * @see erf.permissions.yml
 */
class RegistrationPermissions {

  use BundlePermissionHandlerTrait;
  use StringTranslationTrait;

  /**
   * Get registration permissions.
   *
   * @return array
   *   Permissions array.
   */
  public function permissions() {
    return $this->generatePermissions(RegistrationType::loadMultiple(), [$this, 'buildPermissions']);
  }

  /**
   * Builds a standard list of permissions for a given registration type.
   *
   * @param \Drupal\erf\Entity\RegistrationTypeInterface $registration_type
   *   The registration type.
   *
   * @return array
   *   An array of permission names and descriptions.
   */
  protected function buildPermissions(RegistrationTypeInterface $registration_type) {
    $id = $registration_type->id();
    $args = ['%registration_type' => $registration_type->label()];

    return [
      "view $id registrations" => ['title' => $this->t('%registration_type: View registrations', $args)],
      "create $id registrations" => ['title' => $this->t('%registration_type: Create registrations', $args)],
      "delete $id registrations" => ['title' => $this->t('%registration_type: Delete registrations', $args)],
      "edit $id registrations" => ['title' => $this->t('%registration_type: Edit registrations', $args)],
    ];
  }

}
