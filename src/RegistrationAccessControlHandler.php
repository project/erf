<?php

namespace Drupal\erf;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityHandlerInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Access controller for the Registration entity.
 *
 * @see \Drupal\erf\Entity\Registration.
 */
class RegistrationAccessControlHandler extends EntityAccessControlHandler implements EntityHandlerInterface {

  /**
   * The entity registration session service.
   *
   * @var \Drupal\erf\EntityRegistrationSession
   */
  protected $entityRegistrationSession;

  /**
   * RegistrationAccessControlHandler constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type definition.
   * @param \Drupal\erf\EntityRegistrationSession $entity_registration_session
   *   The entity registration session service.
   */
  public function __construct(EntityTypeInterface $entity_type, EntityRegistrationSession $entity_registration_session) {
    parent::__construct($entity_type);
    $this->entityRegistrationSession = $entity_registration_session;
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
      $entity_type,
      $container->get('erf.session')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\erf\Entity\RegistrationInterface $entity */

    // Anonymous users are special. Ownership is determined by session.
    if ($account->isAnonymous()) {
      $registration_ids = $this->entityRegistrationSession->getRegistrationIds();
      $is_owner = in_array($entity->id(), $registration_ids);
    }
    else {
      $is_owner = $account->id() == $entity->getOwnerId();
    }

    switch ($operation) {
      case 'view':
      case 'view_participant':
        if ($is_owner) {
          return AccessResult::allowedIfHasPermission($account, 'manage own registrations');
        }
        return AccessResult::allowedIfHasPermissions($account, [
          "view {$entity->bundle()} registrations",
          'administer registrations',
        ], 'OR');

      case 'update':
      case 'update_participant':
        if ($is_owner && $account->hasPermission('manage own registrations')) {
          return AccessResult::allowed();
        }
        return AccessResult::allowedIfHasPermissions($account, [
          "edit {$entity->bundle()} registrations",
          'administer registrations',
        ], 'OR');

      case 'delete':
      case 'delete_participant':
        if ($entity->get('locked')->value) {
          return AccessResult::forbidden('Deletion not allowed when locked.');
        }
        if ($is_owner) {
          return AccessResult::allowedIfHasPermission($account, 'manage own registrations');
        }
        return AccessResult::allowedIfHasPermissions($account, [
          "delete {$entity->bundle()} registrations",
          'administer registrations',
        ], 'OR');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermissions($account, [
      "create $entity_bundle registrations",
      'add registrations',
      'administer registrations',
    ], 'OR');
  }

}
