<?php

namespace Drupal\erf\Plugin\views\field;

use Drupal\Core\Form\FormStateInterface;
use Drupal\views\ResultRow;
use Drupal\views\Plugin\views\field\FieldPluginBase;

/**
 * A handler to provide an entity label for associated entities of registrations.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("entity_registration_source_entity_label")
 */
class EntityRegistrationSourceEntityLabel extends FieldPluginBase {

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['link'] = ['default' => TRUE];
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    $form['link'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Link to the associated entity'),
      '#default_value' => $this->options['link'],
      // '#description' => $this->t('Show the comment link in the form used on standard entity teasers, rather than the full entity form.'),
    ];

    parent::buildOptionsForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    /** @var \Drupal\erf\Entity\RegistrationInterface $registration */
    $registration = $values->_entity;

    /** @var \Drupal\core\Entity\ContentEntityInterface $source_entity */
    $source_entity = $registration->getSourceEntity();

    if (empty($source_entity)) {
      return '';
    }

    return $this->options['link'] ? $source_entity->toLink()->toString() : $source_entity->label();
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    // Override the parent query function, since this is a computed field.
  }

}
