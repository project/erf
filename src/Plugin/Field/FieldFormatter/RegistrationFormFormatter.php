<?php

namespace Drupal\erf\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityFormBuilderInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountProxy;
use Drupal\erf\EntityRegistrationSession;
use Drupal\erf\Entity\RegistrationType;
use Drupal\core\Entity\ContentEntityInterface;

/**
 * Plugin implementation of the 'registration_form' formatter.
 *
 * @FieldFormatter(
 *   id = "registration_form",
 *   label = @Translation("Registration Form"),
 *   field_types = {
 *     "entity_reference",
 *   },
 * )
 */
class RegistrationFormFormatter extends FormatterBase implements ContainerFactoryPluginInterface {

  /**
   * The entity form builder.
   *
   * @var \Drupal\Core\Entity\EntityFormBuilderInterface
   */
  protected $entityFormBuilder;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The user account proxy.
   *
   * @var \Drupal\Core\Session\AccountProxy
   */
  protected $currentUser;

  /**
   * The entity registration session service.
   *
   * @var \Drupal\erf\EntityRegistrationSession
   */
  protected $entityRegistrationSession;

  /**
   * Constructs a RegistrationFormFormatter object.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param \Drupal\Core\Entity\EntityFormBuilderInterface $form_builder
   *   The entity form builder.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Session\AccountProxy $current_user
   *   The current user.
   * @param \Drupal\erf\EntityRegistrationSession $entity_registration_session
   *   The entity registration session service.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, EntityFormBuilderInterface $form_builder, EntityTypeManagerInterface $entity_type_manager, AccountProxy $current_user, EntityRegistrationSession $entity_registration_session) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);

    $this->entityFormBuilder = $form_builder;
    $this->entityTypeManager = $entity_type_manager;
    $this->currentUser = $current_user;
    $this->entityRegistrationSession = $entity_registration_session;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('entity.form_builder'),
      $container->get('entity_type.manager'),
      $container->get('current_user'),
      $container->get('erf.session')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    // If no registration type is selected, render nothing.
    if ($items->isEmpty()) {
      return $elements;
    }

    $registration = $this->getRegistration($items->entity, $items->getEntity());
    $registration_form = $this->entityFormBuilder->getForm($registration, 'embedded');

    // If the registration is new (because the current user doesn't have a
    // registration for this source entity) and the user has permission to
    // create this registration type, add the form.
    if ($registration->isNew() && $registration->access('create', $this->currentUser)) {
      $elements[0]['registration_add_form'] = $registration_form;
    }

    // If an existing registration was found for the current user, and they have
    // permission to update this registration, deny access to the form.
    if (!$registration->isNew() && $registration->access('update', $this->currentUser)) {
      $elements[0]['registration_add_form'] = $registration_form;
    }

    return $elements;
  }

  /**
   * Get or create a registration for a given type and source entity.
   *
   * @param \Drupal\erf\Entity\RegistrationType $registration_type
   *   The registration type.
   * @param \Drupal\core\Entity\ContentEntityInterface $source_entity
   *   The source content entity.
   */
  protected function getRegistration(RegistrationType $registration_type, ContentEntityInterface $source_entity) {
    // Try to load an existing registration for this source entity and the
    // current user.
    $props = [
      'type' => $registration_type->id(),
      'user_id' => $this->currentUser->id(),
      'entity_type' => $source_entity->getEntityTypeId(),
      'entity_id' => $source_entity->id(),
      'locked' => 0,
    ];

    // If the user is anonymous, also check the session to see if this source
    // entity is mapped to a registration id. This ensures that anonymous users
    // can only see the registration info that they have created. @see
    // Registration::postSave().
    if ($this->currentUser->isAnonymous()) {
      $props['id'] = $this->entityRegistrationSession->getEntityRegistration($source_entity);

      // If there is no registration session value for this source entity, do
      // not try to load an existing registration.
      if ($props['id']) {
        $registration = $this->entityTypeManager->getStorage('registration')->loadByProperties($props);
      }
      else {
        $registration = FALSE;
      }
    }
    // User is authenticated.
    else {
      $registration = $this->entityTypeManager->getStorage('registration')->loadByProperties($props);
    }

    if ($registration) {
      $registration = reset($registration);
    }
    else {
      // Create a new, empty registration.
      $registration = $this->entityTypeManager->getStorage('registration')->create([
        'type' => $registration_type->id(),
        'entity_type' => $source_entity->getEntityTypeId(),
        'entity_id' => $source_entity->id(),
      ]);
    }

    return $registration;
  }

  /**
   * {@inheritdoc}
   */
  public static function isApplicable(FieldDefinitionInterface $field_definition) {
    // Only allow on fields that reference `registration_type` config entities.
    $storage_def = $field_definition->getFieldStorageDefinition();
    return $storage_def->getSetting('target_type') == 'registration_type';
  }

}
