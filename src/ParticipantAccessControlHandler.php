<?php

namespace Drupal\erf;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;

/**
 * Access controller for the Participant entity.
 *
 * Access permissions for Participants mostly depend on the permissions for the
 * registration that they are part of. The main difference is that participants
 * can potentially be shared between multiple registrations. This must be
 * accounted for in each operation.
 *
 * Note that '_participant' is appended to the associated registration access
 * checks. In RegistrationAccessControlHandler, these non-standard operations
 * will look like 'view_participant', 'update_participant', and
 * 'delete_participant'. For the most part, access control is the same between
 * them (e.g. 'view' and 'view_participant'). This is done primarily so that we
 * can distinguish access checks between registrations and participants in rare
 * cases like hook_ENTITY_TYPE_access. See erf_commerce_registration_access()
 * for an example where participants in unlocked registrations in carts SHOULD
 * be deletable, while the associated registration SHOULD NOT be deletable since
 * it is still part of an order.
 *
 * @see \Drupal\erf\Entity\Participant.
 */
class ParticipantAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\erf\Entity\Participant $entity */

    $registration_permissions = [];

    /** @var \Drupal\erf\Entity\RegistrationInterface $registration */
    foreach ($entity->getRegistrations() as $registration) {
      $registration_permissions[] = $registration->access($operation . '_participant', $account, TRUE);
    }

    // If there are no associated registrations, and thus access permissions,
    // just fall back to admin permissions.
    if (empty($registration_permissions)) {
      return AccessResult::allowedIfHasPermission($account, 'administer registrations');
    }

    /** @var \Drupal\Core\Access\AccessResultInterface $result */
    $result = array_shift($registration_permissions);

    // If there are any remaining registration access permissions, combine them
    // with the first one above, depending on the operation.
    foreach ($registration_permissions as $registration_permission) {
      // If any registration can be viewed, this participant can be viewed.
      if ($operation === 'view') {
        $result = $result->orIf($registration_permission);
      }
      // If any registration cannot be updated, this participant cannot be updated.
      elseif ($operation === 'update') {
        $result = $result->andIf($registration_permission);
      }
      // If any registration cannot be deleted, this participant cannot be deleted.
      elseif ($operation === 'delete') {
        $result = $result->andIf($registration_permission);
      }
    }

    return $result;
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    $permissions = [
      'add registrations',
      'administer registrations',
    ];

    // Get the registration_type(s) that can reference this participant type.
    $registration_type_ids = FieldStorageConfig::loadByName('registration', 'participants')->getBundles();

    foreach ($registration_type_ids as $registration_type_id) {
      $settings = FieldConfig::loadByName('registration', $registration_type_id, 'participants')->getSettings();

      // If this participant type is allowed in this registration type, then add
      // the create permission for the registration type.
      if (in_array($entity_bundle, $settings['handler_settings']['target_bundles'])) {
        $permissions[] = "create $registration_type_id registrations";
      }
    }

    $result = AccessResult::allowedIfHasPermissions($account, $permissions, 'OR');
    return $result;
  }

}
