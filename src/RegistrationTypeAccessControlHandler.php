<?php

namespace Drupal\erf;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Access controller for the Registration Type entity.
 */
class RegistrationTypeAccessControlHandler extends EntityAccessControlHandler {

  /**
   * Allow access to the registration type label.
   *
   * @var bool
   */
  protected $viewLabelOperation = TRUE;

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\erf\Entity\RegistrationTypeInterface $entity*/

    // The registration type label is not privileged information, so this check
    // has has been added to allow labels for registration types to be viewed
    // when a user doesn't have 'administer site configuration' permission. See
    // the `admin_permission` setting in the RegistrationType entity definition.
    if ($operation === 'view label') {
      return AccessResult::allowed();
    }

    return parent::checkAccess($entity, $operation, $account);
  }

}
