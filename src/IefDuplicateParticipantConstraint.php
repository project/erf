<?php

namespace Drupal\erf;

use Drupal\Core\Form\FormStateInterface;

/**
 * Duplicate participant inline entity form detector.
 */
class IefDuplicateParticipantConstraint {

  /**
   * Custom validation callback for participant inline entity forms.
   *
   * @see erf_inline_entity_form_entity_form_alter()
   */
  public static function validate(array &$entity_form, FormStateInterface $form_state) {
    $participants_form_state = $form_state->getValue('participants');
    $ief_form_values = $participants_form_state['form'];
    $triggering_element = $form_state->getTriggeringElement();

    foreach ($form_state->get('inline_entity_form') as $widget) {
      // An empty widget can happen with Simple inline entity form widgets.
      if (empty($widget) || $widget['instance']->getName() !== 'participants') {
        continue;
      }

      $emails = [];
      $collapsed_participants = $widget['entities'];

      // Get the initial, 'collapsed', participant entities.
      foreach ($collapsed_participants as $delta => $participant) {
        $emails[$delta] = $participant['entity']->mail->value;
      }

      // Update the emails for any of the participants with an open inline form.
      if (isset($ief_form_values['inline_entity_form']['entities'])) {
        foreach ($ief_form_values['inline_entity_form']['entities'] as $delta => $item) {
          $emails[$delta] = $item['form']['mail'][0]['value'];
        }
      }

      // If there is an open, un-collapsed form for a new participant, check if
      // it is being used by any of the existing collapsed entities or open
      // update forms.
      $new_participant_form_index = count($collapsed_participants);

      if (isset($ief_form_values[$new_participant_form_index]['mail'][0]['value'])) {
        $new_participant_email = $ief_form_values[$new_participant_form_index]['mail'][0]['value'];

        if (in_array($new_participant_email, $emails)) {
          $form_state->setError($triggering_element, t('A participant with %email already exists for this registration.', [
            '%email' => $new_participant_email,
          ]));
          return;
        }
      }

      // Check for duplicates in the collapsed and/or open update forms.
      $dupes = array_diff_assoc($emails, array_unique($emails));

      foreach ($dupes as $dupe_email) {
        $form_state->setError($triggering_element, t('A participant with %email already exists for this registration.', [
          '%email' => $dupe_email,
        ]));
      }
    }
  }

}
