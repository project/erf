<?php

namespace Drupal\erf\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Entity\EntityStorageInterface;

/**
 * Defines the Registration type entity.
 *
 * @ConfigEntityType(
 *   id = "registration_type",
 *   label = @Translation("Registration type"),
 *   label_collection = @Translation("Registration types"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\erf\RegistrationTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\erf\Form\RegistrationTypeForm",
 *       "edit" = "Drupal\erf\Form\RegistrationTypeForm",
 *       "delete" = "Drupal\erf\Form\RegistrationTypeDeleteForm"
 *     },
 *     "access" = "Drupal\erf\RegistrationTypeAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\erf\RegistrationTypeHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "registration_type",
 *   admin_permission = "administer site configuration",
 *   bundle_of = "registration",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/registrations/registration_types/{registration_type}",
 *     "add-form" = "/admin/registrations/registration_types/add",
 *     "edit-form" = "/admin/registrations/registration_types/{registration_type}/edit",
 *     "delete-form" = "/admin/registrations/registration_types/{registration_type}/delete",
 *     "collection" = "/admin/registrations/registration_types"
 *   }
 * )
 */
class RegistrationType extends ConfigEntityBundleBase implements RegistrationTypeInterface {

  /**
   * The Registration type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Registration type label.
   *
   * @var string
   */
  protected $label;

  /**
   * {@inheritdoc}
   */
  public static function preDelete(EntityStorageInterface $storage, array $entities) {
   // Prevent the deletion of bundle entities if there are existing content
   // entities of this bundle.
   $entity_type_manager = \Drupal::service('entity_type.manager');

   foreach ($entities as $entity) {
     if ($bundle_of = $entity->getEntityType()->getBundleOf()) {
       $bundle_of_entity_type = $entity_type_manager->getDefinition($bundle_of);
       $storage = $entity_type_manager->getStorage($bundle_of);

       $has_data = (bool) $storage->getQuery()
         ->condition($bundle_of_entity_type->getKey('bundle'), $entity->id(), '=')
         ->accessCheck(FALSE)
         ->range(0, 1)
         ->execute();

       if ($has_data) {
         // Use the 409 (Conflict) status code to indicate that the deletion
         // could not be completed due to a conflict with the current state of
         // the target resource.
         throw new EntityStorageException("The '{$entity->label()}' registration type contains data and can not be deleted.", 409);
       }
     }
   }

   parent::preDelete($storage, $entities);
  }

}
