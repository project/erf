<?php

namespace Drupal\erf\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Participant type entities.
 */
interface ParticipantTypeInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
