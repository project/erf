<?php

namespace Drupal\erf\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Registration type entities.
 */
interface RegistrationTypeInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
