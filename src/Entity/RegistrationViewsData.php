<?php

namespace Drupal\erf\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for Registration entities.
 */
class RegistrationViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();
    $base_table = $this->entityType->getBaseTable() ?: $this->entityType->id();

    $data[$base_table]['source_entity_label'] = [
      'title' => t('Associated entity label'),
      'field' => [
        'id' => 'entity_registration_source_entity_label',
      ],
    ];

    return $data;
  }

}
