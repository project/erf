<?php

namespace Drupal\erf\Form;

use Drupal\Core\Entity\EntityConfirmFormBase;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Builds the form to delete Registration type entities.
 */
class RegistrationTypeDeleteForm extends EntityConfirmFormBase {

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to delete %name?', ['%name' => $this->entity->label()]);
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('entity.registration_type.collection');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Delete');
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    try {
      $this->entity->delete();

      $this->messenger()->addStatus($this->t('Deleted the %label registration type.', [
        '%label' => $this->entity->label(),
      ]));
    }
    catch (EntityStorageException $e) {
      // The 409 (Conflict) status code indicates that the deletion could not be
      // completed due to a conflict with the current state of the entity.
      if ($e->getCode() == 409) {
        $this->messenger()->addError($e->getMessage());
      }
      else {
        throw $e;
      }
    }

    $form_state->setRedirectUrl($this->getCancelUrl());
  }

}
