<?php

namespace Drupal\erf\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for Registration edit forms.
 *
 * @ingroup erf
 */
class RegistrationForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    $form_state->set('registration', $this->entity);

    // Prevent edits to fields on locked registrations.
    if ($this->entity->locked->value) {
      foreach (array_keys($this->entity->getFields()) as $field_name) {
        if (isset($form[$field_name]) && $field_name !== 'locked') {
          $form[$field_name]['#disabled'] = TRUE;
        }
      }
    }

    // Change the 'Save' submit button label.
    $form['actions']['submit']['#value'] = $this->entity->isNew() ? $this->t('Register') : $this->t('Update Registration');

    // If this registration form is embedded in another entity, hide the delete
    // action.
    if ($form_state->getBuildInfo()['callback_object']->getOperation() === 'embedded') {
      $form['actions']['delete']['#access'] = FALSE;
    }

    // Configure the cache settings so that anonymous users don't see each
    // others registration form data.
    $form['#cache']['contexts'][] = 'session';
    $form['#cache']['tags'][] = 'registration_list';

    return $form;
  }

  /**
   * {@inheritdoc}
   *
   * This is an additional fallback validator for duplicate participant emails.
   * It only checks collapsed inline entity form participants. These duplicates
   * can happen if a user has a new and a saved participant with the same email,
   * but they have an open edit/update inline form for the saved participant.
   * It's a rare edge case in the Complex inline entity form widget, but is also
   * used with Simple widget.
   *
   * @see IefDuplicateParticipantConstraint::validate()
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);

    foreach ($form_state->get('inline_entity_form') as $widget) {
      if ($widget['instance']->getName() !== 'participants') {
        continue;
      }

      $emails = [];

      foreach ($widget['entities'] as $delta => $participant) {
        $emails[] = $participant['entity']->mail->value;
      }

      $dupes = array_diff_assoc($emails, array_unique($emails));

      foreach ($dupes as $dupe_email) {
        $form_state->setErrorByName('participants', $this->t('A participant with %email already exists for this registration.', [
          '%email' => $dupe_email,
        ]));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = $this->entity;

    $status = parent::save($form, $form_state);

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addStatus($this->t('Registration created.'));
        break;

      default:
        $this->messenger()->addStatus($this->t('Registration saved.'));
    }
  }

}
