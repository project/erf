<?php

namespace Drupal\erf\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Participant entities.
 *
 * @ingroup erf
 */
class ParticipantDeleteForm extends ContentEntityDeleteForm {


}
