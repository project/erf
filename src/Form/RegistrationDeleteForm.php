<?php

namespace Drupal\erf\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Registration entities.
 *
 * @ingroup erf
 */
class RegistrationDeleteForm extends ContentEntityDeleteForm {


}
