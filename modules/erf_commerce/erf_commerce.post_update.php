<?php

/**
 * @file
 * Post update functions for ERF Commerce.
 */

/**
 * Set participants_as_quantity value on existing registration types.
 */
function erf_commerce_post_update_participants_as_quantity3() {
  $registration_type_storage = \Drupal::entityTypeManager()->getStorage('registration_type');
  $entity_field_manager = \Drupal::service('entity_field.manager');

  foreach ($registration_type_storage->loadMultiple() as $registration_type) {
    $registration_type_fields = $entity_field_manager->getFieldDefinitions($registration_type->getEntityType()->get('bundle_of'), $registration_type->id());
    $third_party_settings = $registration_type->getThirdPartySettings('erf_commerce');

    // If the new participants_as_quantity setting is missing and the
    // registration type is integrated with Commerce, set it to true, which is
    // the old behavior before the setting was introduced.
    if (!isset($third_party_settings['participants_as_quantity']) && array_key_exists('product_variation', $registration_type_fields)) {
      $registration_type->setThirdPartySetting('erf_commerce', 'participants_as_quantity', TRUE);
      $registration_type->save();
    }
  }
}
