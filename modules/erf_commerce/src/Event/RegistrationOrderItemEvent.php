<?php

namespace Drupal\erf_commerce\Event;

use Drupal\commerce_order\Entity\OrderItemInterface;
use Drupal\Component\EventDispatcher\Event;
use Drupal\erf\Entity\RegistrationInterface;

/**
 * Fired when an order item is created or updated for a registration.
 */
class RegistrationOrderItemEvent extends Event {

  const EVENT_NAME = 'registration_order_item';

  /**
   * The registration.
   *
   * @var \Drupal\erf\Entity\RegistrationInterface
   */
  public $registration;

  /**
   * The order item.
   *
   * @var \Drupal\commerce_order\Entity\OrderItemInterface
   */
  public $orderItem;

  /**
   * Create a new RegistrationOrderItemEvent.
   *
   * @param \Drupal\erf\Entity\RegistrationInterface $registration
   *   The registration.
   * @param \Drupal\commerce_order\Entity\OrderItemInterface $order_item
   *   The order item associated with the registration.
   */
  public function __construct(RegistrationInterface $registration, OrderItemInterface $order_item) {
    $this->registration = $registration;
    $this->orderItem = $order_item;
  }

}
