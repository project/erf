<?php

namespace Drupal\erf_commerce\Plugin\EntityReferenceSelection;

use Drupal\Core\Entity\EntityReferenceSelection\SelectionPluginBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Routing\CurrentRouteMatch;
use Drupal\Core\Form\FormStateInterface;
use Drupal\commerce_product\Entity\ProductInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Implementation of the VariationSelection Entity Reference Selection plugin.
 *
 * @EntityReferenceSelection(
 *   id = "erf_commerce_variation",
 *   label = @Translation("Product Variations from the current route context"),
 *   entity_types = {"commerce_product_variation"},
 *   group = "erf_commerce_variation",
 *   weight = 0
 * )
 */
class VariationSelection extends SelectionPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The current route match service.
   *
   * @var \Drupal\Core\Routing\CurrentRouteMatch
   */
  protected $routeMatch;

  /**
   * Constructs a new DefaultSelection object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Drupal\Core\Routing\CurrentRouteMatch $current_route_match
   *   The current route match service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, CurrentRouteMatch $current_route_match) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->entityTypeManager = $entity_type_manager;
    $this->routeMatch = $current_route_match;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('current_route_match')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['erf_commerce_variation']['help'] = [
      '#markup' => '<p>' . $this->t('Available variations will be loaded for a product detected from the URL, either via a commerce product parameter (e.g. /product/{commerce_product}) or from the source entity of a registration.') . '</p>',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function getReferenceableEntities($match = NULL, $match_operator = 'CONTAINS', $limit = 0) {
    return $this->getProductVariationsForRoute();
  }

  /**
   * {@inheritdoc}
   */
  public function countReferenceableEntities($match = NULL, $match_operator = 'CONTAINS') {
    $valid_product_variation_ids = [];

    foreach ($this->getProductVariationsForRoute() as $bundle_name => $items) {
      $valid_product_variation_ids = array_merge($valid_product_variation_ids, array_keys($items));
    }

    return count($valid_product_variation_ids);
  }

  /**
   * {@inheritdoc}
   */
  public function validateReferenceableEntities(array $ids) {
    $valid_product_variation_ids = [];

    foreach ($this->getProductVariationsForRoute() as $bundle_name => $items) {
      $valid_product_variation_ids = array_merge($valid_product_variation_ids, array_keys($items));
    }

    return $valid_product_variation_ids;
  }

  /**
   * Get available product variations for the current route context.
   *
   * The variations are filtered by one of the following:
   *
   * - Variations for a product loaded from a commerce route parameter (e.g.
   *   /product/{commerce_product}
   * - From the source entity of a registration, if that source entity is a
   *   product.
   * - From the product_variation of a registration without a source entity.
   * - Variations for products that have an entity reference field to
   *   registration_type config entities, which indicates that the products
   *   integrate with a registration form. Note that this filter can include
   *   variations for multiple products.
   *
   * @param bool $filter
   *   TRUE to filter the variations or FALSE to return them all.
   *
   * @return array
   *   A nested array of entities, the first level is keyed by the variation
   *   bundle (e.g. 'default'), which contains an array of entity labels keyed
   *   by the entity ID.
   */
  private function getProductVariationsForRoute($filter = TRUE) {
    $products = [];
    $return = [];

    // This is probably a registration embedded on a commerce product.
    if ($product_parameter = $this->routeMatch->getParameter('commerce_product')) {
      $products[] = $product_parameter;
    }
    // This would probably be the standalone registration edit form.
    elseif ($registration = $this->routeMatch->getParameter('registration')) {
      $source_entity = $registration->getSourceEntity();

      // If there's a product source entity, this registration was created while
      // embedded on a product.
      if ($source_entity instanceof ProductInterface) {
        $products[] = $source_entity;
      }
      // If the registration was created standalone (e.g. /registration/add/*),
      // it won't have a source entity. Instead, we can just look at the value
      // of the product_variation field and get the product that way.
      elseif ($registration->hasField('product_variation') && !$registration->product_variation->isEmpty()) {
        /** @var \Drupal\commerce_product\Entity\Product $variation_product */
        $products[] = $registration->product_variation->entity->product_id->entity;
      }
    }
    // This is probably the standalone registration add form route.
    elseif ($registration_type_parameter = $this->routeMatch->getRawParameter('registration_type')) {
      // Find all products that have an entity reference field to this registration_type config entity.
      $reg_type_reference_fields = $this->entityTypeManager->getStorage('field_storage_config')->loadByProperties([
        'type' => 'entity_reference',
        'entity_type' => 'commerce_product',
        'settings' => [ 'target_type' => 'registration_type' ],
      ]);

      if (empty($reg_type_reference_fields)) {
        return $return;
      }

      /** @var \Drupal\Core\Entity\Query\Sql\Query $query */
      $query = $this->entityTypeManager->getStorage('commerce_product')->getQuery();
      $query->accessCheck(TRUE);
      $query->condition('status', 1);
      $group = $query->orConditionGroup();
      $query->condition($group);

      /** @var \Drupal\field\Entity\FieldStorageConfig $field_storage */
      foreach ($reg_type_reference_fields as $field_storage) {
        $group->condition($field_storage->getName(), $registration_type_parameter);
      }

      $product_ids = $query->execute();
      $products = $this->entityTypeManager->getStorage('commerce_product')->loadMultiple($product_ids);
    }

    if (empty($products)) {
      return $return;
    }

    /** @var \Drupal\commerce_product\ProductVariationStorageInterface $variation_storage */
    $variation_storage = $this->entityTypeManager->getStorage('commerce_product_variation');

    foreach ($products as $product) {
      if ($product->variations->isEmpty()) {
        continue;
      }

      // ProductVariationStorage::loadEnabled() is a bit better than
      // $product->getVariations() because it runs the
      // `ProductEvents::FILTER_VARIATIONS` event, which can be used to filter out
      // ineligable variations.
      $filtered_variations = $variation_storage->loadEnabled($product);

      foreach ($filtered_variations as $product_id => $variation) {
        $return[$variation->bundle()][$product_id] = $variation->label();
      }
    }

    return $return;
  }

}
